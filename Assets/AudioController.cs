﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {
    public AudioClip MainTheme;
    public AudioClip LevelTheme;
    public AudioClip MenuExit;
    public AudioClip MenuReenter;
    public AudioSource main;
    public AudioSource secondary;
    public MainMenuController menu;

    public void playClip(AudioClip clip, bool loop, bool source)
    {
        if(source)
        {
            main.loop = loop;
            main.clip = clip;
            main.Play();
        } else {
            if(menu != null && !menu.IsOpen)
            {
                if(!secondary) { return; }
                secondary.loop = loop;
                secondary.PlayOneShot(clip);
            }
        }
    }

    public void playClip(AudioClip clip, bool loop, bool source, float volume)
    {
        if( volume <= 0 || volume >=1)
        {
            volume = 1;
        }

        if (source)
        {
            main.loop = loop;
            main.clip = clip;
            main.volume = volume;
            main.Play();
        }
        else
        {
            if (menu != null && !menu.IsOpen)
            {
                if (!secondary) { return; }
                secondary.loop = loop;
                secondary.volume = volume;
                secondary.PlayOneShot(clip);
            }
        }
    }

    public void playClip(AudioClip clip, bool loop, bool source, float volume, bool playInMenu)
    {
        if (volume <= 0 || volume >= 1)
        {
            volume = 1;
        }


        if(playInMenu == false)
        {
            //fix this duplication
            if (menu != null && !menu.IsOpen)
            {
                if (source)
                {
                    main.loop = loop;
                    main.clip = clip;
                    main.volume = volume;
                    main.Play();
                }
                else
                {
                    if (!secondary) { return; }
                    secondary.loop = loop;
                    secondary.volume = volume;
                    secondary.PlayOneShot(clip);
                }
            }
        } else
        {
            //fix this duplication
            if (source)
            {
                main.loop = loop;
                main.clip = clip;
                main.volume = volume;
                main.Play();
            }
            else
            {
                if (!secondary) { return; }
                secondary.loop = loop;
                secondary.volume = volume;
                secondary.PlayOneShot(clip);
            }
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void playIntroSong()
    {
        if(main)
        {
            main.clip = MainTheme;
            main.loop = true;
            main.Play();
        }
    }

    public void playLevelTheme()
    {
        if(main)
        {
            main.clip = LevelTheme;
            main.loop = true;
            main.Play();
        }
    }

    public void StopMusic()
    {
        if(main)
        {
            main.Stop();
            main.clip = null;
        }
    }

    public void playExitSFX()
    {
        if(main)
        {
            main.clip = MenuExit;
            main.loop = false;
            main.Play();
            Invoke("playLevelTheme", 1f);
        }
    }

    public void playReenter()
    {
        if(main)
        {
            main.clip = MenuReenter;
            main.Play();
            main.loop = false;
            Invoke("playIntroSong", 1f);
        }
    }
}
