﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatController : MonoBehaviour {
    public AudioController audioController;
    public GameObject projectile = null;
    public Transform projectileSpawn;//B
    public Transform projectileSpawnA;
    public float speed = 6f;
    public CharacterController2D player;
    public Transform ProjectileGarbageCollector;
    public AudioClip gunShotSound;
    public MainMenuController menu;
    float coolDown = 0f;


    private void Start()
    {
        if(gunShotSound == null)
        {
            //throw new System.NotImplementedException("please ad a gunshot AudioClip to the CombatController");
        } 
    }

    //single shot
    public void Fire()
    {
        if(projectile != null && menu != null && menu.IsOpen == false)
        {
            var newProjectile = Instantiate(projectile, ProjectileGarbageCollector);
            newProjectile.transform.position = projectileSpawn.transform.position;
            var direction = Vector2.right;
            Rigidbody2D rb = newProjectile.GetComponent<Rigidbody2D>();
            //face projectile to last cursor pos
            //fire in direction
            rb.velocity = projectileSpawnA.right + projectileSpawn.right * speed; //Richtungsvektor Waffe
            audioController.playClip(gunShotSound, false, false, 0.3f); //use secondary audiosource to play sound
        }
    }

    //auto fire
    public void Fire(float fireSpeed, float fireRate)
    {
        //firerate has to be at leat 6 (for some reaseon)
        if (Time.time > coolDown)
        {
            if (projectile != null && menu != null && menu.IsOpen == false)
            {
                if (fireSpeed < 6)
                    fireSpeed = 6;

                var newProjectile = Instantiate(projectile, ProjectileGarbageCollector);
                newProjectile.transform.position = projectileSpawn.transform.position;
                var direction = Vector2.right;
                Rigidbody2D rb = newProjectile.GetComponent<Rigidbody2D>();
                //fire bullelt from weapon
                rb.velocity = projectileSpawnA.right + projectileSpawn.right * fireSpeed; //Richtungsvektor Waffe
                audioController.playClip(gunShotSound, false, false, 0.3f); //use secondary audiosource to play sound

                //set cooldown
                coolDown = Time.time + fireRate;
            }
        }
    }
}
