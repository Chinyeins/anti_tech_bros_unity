﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// deactivates HasGun bool in Animator of MainChar
/// </summary>
public class NoGunArea : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            var d = collision.gameObject.GetComponent<CharacterController2D>();
            d.hasGun = false;


        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            var d = collision.gameObject.GetComponent<CharacterController2D>();
            d.hasGun = true;


        }
    }
}
