﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {
    public CharacterController2D controller;
    private Vector2 direction;
    private float runSpeed = 1f;
    public const float DEFAULT_RUN_SPEED = 1f;
    public const float DEFAULT_SPRINT_SPEED = 1.5f;

    // Update is called once per frame
    void Update () {
        getInput();
    }

    private void FixedUpdate()
    {
        //move character
        controller.Move(Time.fixedDeltaTime * runSpeed, direction);
        controller.SetAnimationState(direction);
    }

    private void getInput()
    {
        direction = Vector2.zero; //resets movement after ever itertion
        runSpeed = DEFAULT_RUN_SPEED;

        if (Input.GetKey(KeyCode.W))
        {
            //forward
            direction += Vector2.right;
        }

        if (Input.GetKey(KeyCode.S))
        {
            //backwards -> Not necessarry
            direction += Vector2.left;
        }

        if(Input.GetKey(KeyCode.A))
        {
            direction += Vector2.up;
        }

        if (Input.GetKey(KeyCode.D))
        {
            direction += Vector2.down;
        }

        if(Input.GetKey(KeyCode.LeftShift))
        {
            runSpeed = DEFAULT_SPRINT_SPEED;
        }
    }
}
