﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {
    private int _score = 0;
    public int Score { get; set; }
    public Text _scoreText;
    public AudioClip scoreUpSFX;
    public AudioController audioController;
    public GameObject scoreUI;

    private void Start()
    {
        audioController = GetComponent<AudioController>();
    }

    public void addScore(int val)
    {
        Score += val;
        audioController.playClip(scoreUpSFX, false, false);
    }

    private void FixedUpdate()
    {
        _scoreText.text = Score.ToString();
    }


    #region events
    private void OnEnable()
    {
        PlayerHealthController.OnScoreAdd += addScore;
    }

    private void OnDisable()
    {
        PlayerHealthController.OnScoreAdd -= addScore;
    }

    public void setUIDisplay(bool v)
    {
        scoreUI.SetActive(v);
    }
    #endregion
}
