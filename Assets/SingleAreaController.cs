﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleAreaController : MonoBehaviour
{
    public GameObject SpawnPoint;
    public BoxCollider2D AreaBounds;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region Getter/ setter
    public Transform getSpawnPoint()
    {
        return SpawnPoint.transform;
    }

    public Bounds getAreaBounds()
    {
        return AreaBounds.bounds;
    }

    #endregion
}
