﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using SimpleJSON;


/// <summary>
/// Handles Game States, Player Respawn and Level inits
/// </summary>
public class GameController : MonoBehaviour {
    private bool isOpen;
    public bool IsOpen { get; set; }
    public GameObject menuPanel;
    public Button StartBtn;
    public Button ExitBtn;
    public AudioController m_audio;
    public bool isStartup = true;
    public PlayerHealthController healthController;
    public MainMenuController menu;
    public Transform SpawnPointPlayer;
    public GameObject PlayerOBJ;
    public CharacterController2D Player;
    public GameOverMenuController gameOverMenu;
    public ScoreController score;
    public GameObject highScoreUIContent;
    public GameObject scoreTextPrefab;
    public AreaController areaController;

    // Use this for initialization
    void Start () {
        //get


        /*if (SpawnPointPlayer == null)
            throw new System.DllNotFoundException("SpawnPoint for player is not set.");*/
    }
	
    /// <summary>
    /// Resets the Game
    /// </summary>
	public void StartGame()
    {
        if(isStartup)
        {
            areaController.generateAreas();

            ResetPlayer();
            menu.closeMenu();
            healthController.healthBar.setUIDisplay(true);
            score.setUIDisplay(true);
        } else
        {
            ResumeGame();
        }

        isStartup = false;
    }

    public void ResumeGame()
    {
        menu.closeMenu();
        healthController.healthBar.setUIDisplay(true);
        score.setUIDisplay(true);
    }

    public void PauseGame()
    {
        if (!IsOpen && isStartup == false)
            menu.openMenu();
        else
            menu.closeMenu();
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ResetPlayer()
    {
        if (Player != null)
        {
            SpawnPlayer();
            healthController.healthBar.setUIDisplay(true);
            healthController.initPlayerStats();
        }
    }

    public void SpawnPlayer()
    {
        if(Player != null)
        {
            //get a spawn point 
            SpawnPointPlayer = areaController.getArea(0, 0).GetComponent<SingleAreaController>().getSpawnPoint();
            Player.transform.position = SpawnPointPlayer.transform.position;
        }
    }

    /// <summary>
    /// Spawn Player
    /// </summary>
    public void StartLevel()
    {
        ResetPlayer();
        
    }

    /// <summary>
    /// Game Over Menu
    /// </summary>
    public void GameOver()
    {
        gameOverMenu.openMenu();
        Player.healthController.healthBar.setUIDisplay(false);
        Player.animator.SetBool("isDead", true);
    }

    // Update is called once per frame
    void Update()
    {
        //get Escape Key	
        if (Input.GetKeyDown(KeyCode.Escape) && isStartup == false)
        {
            PauseGame();
        }
    }

    /// <summary>
    /// Get all highscores for main menu
    /// </summary>
    public List<KeyValuePair<string, int>> getHighScores()
    {
        var highScoreString = PlayerPrefs.GetString("HighScores");
        List<KeyValuePair<string, int>> HighScores = null;
        if (!highScoreString.Equals("{}"))
        {
            try
            {
                HighScores = JsonConvert.DeserializeObject<List<KeyValuePair<string, int>>>(highScoreString);
            } catch (Exception e)
            {
                //
                Debug.LogError("Error fetching PlayersHighscores");
            }
            
        }

        return HighScores;
    }

    /// <summary>
    /// Save highscore in playerprefs
    /// </summary>
    public void saveHighScore(string playerName, int playerScore) 
    {
        var highScoreString = PlayerPrefs.GetString("HighScores");
        if(!highScoreString.Equals("{}"))
        {
            try
            {
                List<KeyValuePair<string, int>> HighScores = JsonConvert.DeserializeObject<List<KeyValuePair<string, int>>>(highScoreString);
                HighScores.Add(new KeyValuePair<string, int>(playerName, playerScore));
                highScoreString = JsonConvert.SerializeObject(HighScores);

                PlayerPrefs.SetString("HighScores", highScoreString);
                PlayerPrefs.Save();
            } catch (Exception e)
            {
                Debug.LogError("Error saving PlayersHighscores");
            }
        } else
        {
            List<KeyValuePair<string, int>> HighScores = new List<KeyValuePair<string, int>>();
            HighScores.Add(new KeyValuePair<string, int>(playerName, playerScore));
            highScoreString = JsonConvert.SerializeObject(HighScores);

            PlayerPrefs.SetString("HighScores", highScoreString);
            PlayerPrefs.Save();
        }
    }

    public void OpenHighScore()
    {
        //openmenu
        menu.menuPanel.SetActive(false);
        menu.highScorePanel.SetActive(true);

        List<KeyValuePair<string, int>> HighScores = getHighScores();

        if(HighScores != null)
        {
            Transform lastPos = gameObject.transform;
            foreach (KeyValuePair<string, int> hScore in HighScores)
            {
                //add HighScores to listview in Highscore view (list)
                GameObject playerScore = Instantiate(scoreTextPrefab, highScoreUIContent.transform, false);
                playerScore.GetComponent<Text>().text = "" + hScore.Key + " - " + hScore.Value.ToString();
            }
        }
    }
}
