﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Creates Random Areas
/// </summary>
public class AreaController : MonoBehaviour
{
    public int AreaSize = 10;
    public ArrayList[,] Areas = null;
    public Object[] VariantList = null;


    // Start is called before the first frame update
    void Start()
    {
        Areas = new ArrayList[AreaSize, AreaSize];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region (Getter / Setter)
    public GameObject getArea(int x, int y) 
    {
        GameObject go = null;
        go = (GameObject) Areas[x, y][0];
        return go;
    }


    #endregion



    /// <summary>
    /// Setup PlayAreas initially at GameStart
    /// </summary>
    public void generateAreas()
    {
        Object[] PrefabList = Resources.LoadAll("Areas");
        VariantList = PrefabList;


        for(int i = 0; i < AreaSize; i++)
        {
            for(int j = 0; j < AreaSize; j++)
            {
                //create Initial Area obj
                GameObject prefab = null;

                //get random area prefab
                int rIndex = Random.Range(0, PrefabList.Length);
                prefab = PrefabList[rIndex] as GameObject;

                GameObject initArea = Instantiate( prefab );
                initArea.transform.SetParent(transform);
                Vector3 initPos = initArea.transform.position;
                //shift area - right
                if (j != 0)
                {
                    //shift Area to the right + size of last one
                    float size = initArea.GetComponent<SingleAreaController>().getAreaBounds().size.x;
                    initPos.x = size * j;
                }

                //shift area - bottom
                if (i != 0)
                {
                    //shift Area to the bottom + size of last one
                    float size = initArea.GetComponent<SingleAreaController>().getAreaBounds().size.y;
                    initPos.y = size * i;
                }

                //change pos of Area
                initArea.transform.position = initPos;

                //add Area to list
                Areas[i, j] = new ArrayList();
                Areas[i, j].Add(initArea);
            }
        }
    }
}
