﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour, IPlayer
{
    #region variables
    [SerializeField] private float m_JumpForce = 400f;                          // Amount of force added when the player jumps.
    [Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;          // Amount of maxSpeed applied to crouching movement. 1 = 100%
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
    public bool hasGun = false;
    public PlayerHealthController healthController;
    public GameController game;
    public float fireRate = 6f;
    public float fireSpeed = 6f;
    private Vector3 _mousePos;
    public Vector3 CursorPosition { get; set; }
    private float _mouseAngle;
    public float CursorAngle { get; set; }
    public GameObject CameraFollow;

    [SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
    [SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.
    [SerializeField] private Transform m_CeilingCheck;                          // A position marking where to check for ceilings
    [SerializeField] private Collider2D m_CrouchDisableCollider;                // A collider that will be disabled when crouching
    [Range(0, 10)] [SerializeField] private float m_runSpeed = 5f;

    private Vector2 direction;

    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    private bool m_Grounded;            // Whether or not the player is grounded.
    const float k_CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
    private Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    private bool m_FacingUp = true;  // For determining which way the player is currently facing.
    private Vector3 m_Velocity = Vector3.zero;

    public Animator animator;

    [Header("Events")]
    [Space]

    public UnityEvent OnLandEvent;

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }

    public BoolEvent OnCrouchEvent;
    private bool m_wasCrouching = false;

    public CombatController combatController;
    public MainMenuController mainMenu;
    #endregion

    float powerUpGunsCooldown = 0f;

    public void SetAnimationState(Vector2 direction)
    {
        //bugfix - overlaping states
        if(animator.GetBool("isGunWalking") && animator.GetBool("isWalking")) {
            animator.SetBool("isGunWalking", false);
            animator.SetBool("isWalking", false);
        }

        if (hasGun == true)
        {
            //gun run
            if (direction == Vector2.zero)
                animator.SetBool("isGunWalking", false);
            else
                animator.SetBool("isGunWalking", true);
        } else
        {
            
            if (direction == Vector2.zero)
                animator.SetBool("isWalking", false);
            else
                animator.SetBool("isWalking", true);
        }
        
    }
    


    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();

        if (OnCrouchEvent == null)
            OnCrouchEvent = new BoolEvent();

        animator = GetComponent<Animator>();
    }


    private void FixedUpdate()
    {
        bool wasGrounded = m_Grounded;
        m_Grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
                if (!wasGrounded)
                    OnLandEvent.Invoke();
            }
        }

        //update player to cursor direction
        faceToCursor();

        //check if player fires
        if (Input.GetMouseButton(0) && mainMenu.IsOpen == false)
        {
            //auto fire
            if (!animator.GetBool("isDead") && hasGun)
            { 
                //TODO: add fireRate dynamic here (by powerups)
                combatController.Fire(fireSpeed, fireRate);
            }
        }

        if(Input.GetMouseButtonDown(0) && mainMenu.IsOpen == false)
        {
            //check if player fires
            if (Input.GetMouseButton(0) && mainMenu.IsOpen == false)
            {
                //fire bullet
                if (!animator.GetBool("isDead") && hasGun)
                {
                    //single fire
                    combatController.Fire();
                }
            }
        }
    }


    public void Move(float speed, Vector2 direction)
    {
        if(!animator.GetBool("isDead"))
        {
            transform.Translate(speed * m_runSpeed * direction);
            AlignToMouse(Vector2.left);
        }
    }

    //deprecated - we use mouse for alignment
    private void AlignToMouse(Vector2 direction)
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;
        m_FacingUp = !m_FacingUp;        
    }

    private void faceToCursor()
    {
        if(!animator.GetBool("isDead"))
        {   
            //face char to cursor
            Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePos);
            lookPos = lookPos - transform.position; // Richtungsvektor -> "GuckRichtung"
            float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg; //Winkel
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            //face camera to lokPos Vector with Rotation by angle
           // CameraFollow.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            CursorPosition = lookPos;
            CursorAngle = angle;
        }
    }

    public void killPlayer()
    {
        animator.SetBool("isDead", true);
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0;
        Destroy(GetComponent<BoxCollider2D>());
        GetComponent<SpriteRenderer>().sortingLayerName = "Background";
        //add points to ScoreControll (needs to  be written first ...)

        game.GameOver();
    }

    public void respawnPlayer()
    {
        //
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {
            EnemyController2D enemy = collision.gameObject.GetComponent<EnemyController2D>();
            if (enemy != null)
                healthController.receiveDamage(enemy.HitDamage);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            var coolDown = Time.time + 1f;
            if(Time.time > coolDown)
            {
                EnemyController2D enemy = collision.gameObject.GetComponent<EnemyController2D>();
                if (enemy != null)
                    healthController.receiveDamage(enemy.HitDamage);
            }
        }
    }

    /// <summary>
    /// Change Gunplay -> fireRate and projectileSpeed
    /// </summary>
    protected void changeFireRateAndSpeed(float fireRate, float fireSpeed, float buffTime, int HP)
    {
        var oldF = this.fireRate;
        var oldS = this.fireSpeed;
        //save current values
        this.fireRate = fireRate;
        this.fireSpeed = fireSpeed;
        //add points for gaining HP Boost
        healthController.receiveHP(HP);
        StartCoroutine(resetPowerUpFireRateAndSpeed(oldF, oldS, buffTime));
    }


    /// <summary>
    /// Reset to old values - PowerUp_1 Reset
    /// </summary>
    /// <param name="oldFireRate"></param>
    /// <param name="oldFireSpeed"></param>
    /// <param name="coolDown"></param>
    /// <returns></returns>
    IEnumerator resetPowerUpFireRateAndSpeed(float oldFireRate, float oldFireSpeed, float coolDown)
    {
        yield return new WaitForSeconds(coolDown);
        //reset here
        fireRate = oldFireRate;
        fireSpeed = oldFireSpeed;
    }

    public void OnEnable()
    {
        PowerUP_1.OnPowerUp += changeFireRateAndSpeed;
    }

    public void OnDisable()
    {
        PowerUP_1.OnPowerUp -= changeFireRateAndSpeed;
    }
}