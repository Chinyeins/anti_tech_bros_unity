﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Garbage collector
/// </summary>
public class ProjectileGC : MonoBehaviour {
    public GameObject pojectileGarbageCollectorObject;
    public int MAX_PROJECTILES = 100;

    private void Start()
    {
        if(pojectileGarbageCollectorObject == null)
        {
            throw new System.NotImplementedException("Please add a empty Gameobject to the Scene for collecting all projectiles");
        }
    }

    private void FixedUpdate()
    {

        if(pojectileGarbageCollectorObject.transform.childCount > MAX_PROJECTILES)
        {
            removeUntilMax();
        }
    }

    private void removeUntilMax()
    {
        int index = pojectileGarbageCollectorObject.transform.childCount - MAX_PROJECTILES;
        if(index > 0) {
            for (int i = index; i > 0; i--)
            {
                Transform projT = pojectileGarbageCollectorObject.transform.GetChild(i);
                Destroy(projT.gameObject);
            }
        }
    }
}
