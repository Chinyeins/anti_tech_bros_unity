﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController : MonoBehaviour {
    public bool MakeFollowPlayer = true;
    public const string PLAYER_TAG = "Player";
    public GameObject EnemyPrefab;
    [SerializeField] public int MaxEnemies = 4;
    [SerializeField] public float TriggerRadius = 4f;
    [Range(1, 10)] [SerializeField] public float SpawnIntervall = 3f;
    public float nextSpawn = 0;
    public int spawnedEnemies = 0;
    public bool hasTriggered = false;
    public float moveSpeed = 2f;

    // Use this for initialization
    void Start () {
        if(EnemyPrefab == null)
        {
            throw new System.NotImplementedException("Please select a Enemy Prefab for this SpawnPoint + " + gameObject.name);
        }

        //Hide SpawnPoint Sprite in Game
        if (Application.isPlaying)
        {
            //gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        CircleCollider2D col = GetComponent<CircleCollider2D>();
        col.radius = TriggerRadius;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (Time.time > nextSpawn)
            spawnEnemy(EnemyPrefab);
    }

    public void spawnEnemy(GameObject prefab)
    {
        if(spawnedEnemies < MaxEnemies && hasTriggered)
        {
            //spawn enemy
            Vector2 spawnPos = getRandoSpawnPosition();
            GameObject enemy = Instantiate(EnemyPrefab, gameObject.transform);
            enemy.transform.localPosition = spawnPos; //set spawn pos

            if(MakeFollowPlayer)
                enemy.GetComponent<EnemyController2D>().FollowPlayer = true;
            else
                enemy.GetComponent<EnemyController2D>().FollowPlayer = false;

            //set runspeed
            enemy.GetComponent<EnemyController2D>().RunSpeed = moveSpeed;

            //set cooldown
            nextSpawn = Time.time + SpawnIntervall;
            spawnedEnemies++;
        }
    }


    /**
     * Returns a random position (localScale) of the Spawn Position CircleCollider
    */
    private Vector2 getRandoSpawnPosition()
    {
        BoxCollider2D col = GetComponent<BoxCollider2D>();
        Bounds bounds = col.bounds;
        float xRange = bounds.max.x - bounds.min.x;
        float yRange = bounds.max.y - bounds.min.y;

        var ranX = Random.Range(xRange, xRange+(xRange/2));
        var ranY = Random.Range(yRange, yRange+(yRange/2));

        return new Vector2(ranX, ranY);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //check for player collission
        if (collision.gameObject.tag == PLAYER_TAG)
        {
            nextSpawn = Time.time + SpawnIntervall;
            hasTriggered = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //check for player collission
        if (collision.gameObject.tag == PLAYER_TAG)
        {
            hasTriggered = false;
        }
    }

}
