﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveController : MonoBehaviour {
    public EnemyController2D controller;
    private Vector2 direction;
    private bool playerInsight = false;
    public bool IsPLayerInSight { get; set; }


    private void FixedUpdate()
    {
        //move character
        getInput(); //check if play is in sight
        controller.Move(Time.fixedDeltaTime, direction);
        controller.SetAnimationState(direction);
    }

    private void getInput()
    {
        direction = Vector2.zero; //stop player
        if (IsPLayerInSight)
        {
            if(controller.FollowPlayer) //dont walk to player if not true
                direction += Vector2.right; //go forwad 
        }
    }
}
