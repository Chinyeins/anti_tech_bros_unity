﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverMenuController : MonoBehaviour {
    private bool isOpen;
    public bool IsOpen { get; set; }
    public GameObject menuPanel;
    public Button StartBtn;
    public Button ExitBtn;
    public AudioController m_audio;
    public bool isStartup = false;
    public GameController game;
    public GameObject gameCursor;
    public AudioClip GameOverTheme;
    public InputField playerNameInputField;

    private void Start()
    {
        isStartup = true;
        menuPanel.SetActive(false);
 
        StartBtn.onClick.AddListener(() =>
        {
            //save highscore
            game.saveHighScore(playerNameInputField.text,
                game.score.Score);
            //reset scene
            isStartup = true;
            SceneManager.LoadScene("FirstScene");
        });

        ExitBtn.onClick.AddListener(() =>
        {
            game.QuitGame();
        });
    }

    public void openMenu()
    {
        Cursor.visible = true;
        gameCursor.SetActive(false);
        m_audio.StopMusic();
        m_audio.playClip(GameOverTheme, false, false);
        IsOpen = true;
        menuPanel.SetActive(true);
    }

    public void closeMenu()
    {
        Cursor.visible = false;
        gameCursor.SetActive(true);
        m_audio.StopMusic();
        IsOpen = false;
        menuPanel.SetActive(false);
        m_audio.playExitSFX();
    }
}
