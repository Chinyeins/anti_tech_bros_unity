﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic methods tha a player should have
/// </summary>
public interface IPlayer  {
    void killPlayer();
    void respawnPlayer();
}
