﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : MonoBehaviour {
    public HealthBar healthBar;
    public AudioController audioController;
    private float _playerHealth = 1f;
    private const int MAX_PLAYER_BAR_VAL = 1; //100% bar 
    public float PlayerHealth { get; set; } //in percent

    /// <summary>
    /// Players general health value
    /// </summary>
    public int PLAYER_HEALTH = 250;
    private int _healt = 250; //standard value
    public int Health { get; set; }
    public MainMenuController menu;
    public AudioClip playerHitSound;
    public AudioClip playerDeadSound;

    #region events
    public delegate void ScoreAction(int damage);
    public static event ScoreAction OnScoreAdd;
    #endregion

    private void Start()
    {
        Health = PLAYER_HEALTH;
    }

    /**
     * Calc damage
    */
    public void receiveDamage(int val)
    {
        //check health
        if(Health <= 0)
        {
            killPlayer();
        }

        if(val > 0)
        {
            //make damage
            Health -= val;
            if (Health <= 0)
                Health = 0;

            //update healthbar, if player has one
            if (healthBar != null)
            {
                float curHealthInPercent = (float)(((float)MAX_PLAYER_BAR_VAL / (float)PLAYER_HEALTH) * (float)Health);
                setPlayerHealth(curHealthInPercent);
                //play healthbar Sound
                //audioController.playClip(playerDeadSound, false, false);
                //play damage sound
                audioController.playClip(playerHitSound, false, false);
                //add score for damage
                float sc = ((float)val / 100) * 25;
                OnScoreAdd((int)sc); //only add 25% for damage on enemies
            }
        }
    }

    /**
     * Calc damage
    */
    public void receiveHP(int val)
    {
        if (val > 0)
        {
            //make HP boost
            if((Health+ val) <= PLAYER_HEALTH) //dont increase players max Health 
                Health += val;
            //update healthbar, if player has one
            if (healthBar != null)
            {
                float curHealthInPercent = (float)(((float)MAX_PLAYER_BAR_VAL / (float)PLAYER_HEALTH) * (float)Health);
                setPlayerHealth(curHealthInPercent);
                //play healthbar Sound
                //audioController.playClip(playerDeadSound, false, false);
                //play damage sound
                audioController.playClip(playerHitSound, false, false);
                //add score for HP BOOST
                OnScoreAdd(val);
            }
        }
    }

    public void initPlayerStats()
    {
        setPlayerHealth(MAX_PLAYER_BAR_VAL);
    }

    /**
     * Set player Health and HealthBar - init
    */
    public void setPlayerHealth(float val)
    {
        PlayerHealth = val;
        if(healthBar != null)
        {
            healthBar.setHealthBar(val);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //detect porjectile - dont give player damage by Projectiles
        if(gameObject.tag != "Player" && collision.gameObject.tag == "Projectile")
        {
            var projectile = collision.gameObject.GetComponent<ProjectileController>();
            if(projectile != null)
            {
                receiveDamage(projectile.Damage);
            }
        }
    }

    //TODO: FIX THIS! -> use Visitor pattern here!
    private void killPlayer()
    {
        CharacterController2D mainChar = gameObject.GetComponent<CharacterController2D>();
        EnemyController2D enemy = gameObject.GetComponent<EnemyController2D>();

        if (mainChar != null)
        {
            mainChar.killPlayer();
        }

        if (enemy!= null)
        {
            PlayerHealthController phEnemy = enemy.GetComponent<PlayerHealthController>();
            if(phEnemy != null)
            {
                OnScoreAdd(phEnemy.PLAYER_HEALTH); //add enemies HP as score
                enemy.killPlayer();
            }
        }
    }

    public void ShowHealth(int damage)
    {
        Debug.Log("Player health: " + Health.ToString());
    }

    private void OnEnable()
    {
        PlayerHealthController.OnScoreAdd += ShowHealth;
    }

    private void OnDisable()
    {
        PlayerHealthController.OnScoreAdd -= ShowHealth;
    }
}
