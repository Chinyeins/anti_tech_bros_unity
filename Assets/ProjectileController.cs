﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {
    public int projectileDamage = 50;
    private int _damage = 0;
    public int Damage { get; set; }
    public float vanishAfterSeconds = 1f;


    private void Start()
    {
        Damage = projectileDamage;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}
