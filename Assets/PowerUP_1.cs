﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUP_1 : MonoBehaviour {
    public float spinSpeed = 5f;
    public AudioClip PowerUpClip;
    public AudioController audioController;
    public int PlayerHPBoost = 1000;


    [Space]
    public float fireRate = 0.1f;
    public float fireSpeed = 12f;
    public float powerUpTime = 10f;

    #region events
    public delegate void PowerUp(float fireRate, float fireSpeed, float buffTime, int HP);
    public static event PowerUp OnPowerUp;
    #endregion
	
	// Update is called once per frame
	void FixedUpdate () {
        gameObject.transform.Rotate(new Vector3(0, 0, Time.deltaTime * spinSpeed * 25));
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //fire powerUp event and destroy powerup
        
        if(collision.gameObject.tag == "Player")
        {
            OnPowerUp(fireRate, fireSpeed, powerUpTime, PlayerHPBoost);
            audioController.playClip(PowerUpClip, false, false, 0.5f);
            //notify ScorController
            
            Destroy(gameObject);
        }
    }
}
