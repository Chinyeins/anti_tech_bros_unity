﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class EnemyController2D : MonoBehaviour, IPlayer
{
    public AudioClip killSound;
    public int HitDamage = 10;
    public float VanishAfterSeconds = 10f;
    public bool FollowPlayer = true;
    [SerializeField] private float m_JumpForce = 400f;                          // Amount of force added when the player jumps.
    [Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;          // Amount of maxSpeed applied to crouching movement. 1 = 100%
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // How much to smooth out the movement
    public GameObject CurrentPlayer;
    public float RunSpeed = 1f;

    internal void SetAnimationState(Vector2 direction)
    {
        if (direction == Vector2.zero)
            animator.SetBool("isWalking", false);
        else
            animator.SetBool("isWalking", true);
    }

    [SerializeField] private bool m_AirControl = false;                         // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask m_WhatIsGround;                          // A mask determining what is ground to the character
    [SerializeField] private Transform m_GroundCheck;                           // A position marking where to check if the player is grounded.
    [SerializeField] private Transform m_CeilingCheck;                          // A position marking where to check for ceilings
    [SerializeField] private Collider2D m_CrouchDisableCollider;                // A collider that will be disabled when crouching
    [Range(0, 10)] [SerializeField] private float m_runSpeed = 5f;

    private Vector2 direction;

    const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
    private bool m_Grounded;            // Whether or not the player is grounded.
    const float k_CeilingRadius = .2f; // Radius of the overlap circle to determine if the player can stand up
    private Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    private bool m_FacingUp = true;  // For determining which way the player is currently facing.
    private Vector3 m_Velocity = Vector3.zero;

    public Animator animator;

    [Header("Events")]
    [Space]

    public UnityEvent OnLandEvent;

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }

    public BoolEvent OnCrouchEvent;
    private bool m_wasCrouching = false;

    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();

        if (OnCrouchEvent == null)
            OnCrouchEvent = new BoolEvent();

        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        bool wasGrounded = m_Grounded;
        m_Grounded = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;
                if (!wasGrounded)
                    OnLandEvent.Invoke();
            }
        }

        //update player to cursor direction
        faceToPlayer();
    }


    public void Move(float speed, Vector2 direction)
    {
        //dont follow player if not true
        if(FollowPlayer)
        {
            if (animator.GetBool("isDead") == false)
            {
                transform.Translate(speed * RunSpeed * direction);
                AlignToMouse(Vector2.left);
            }
        }
    }

    //deprecated - we use mouse for alignment
    private void AlignToMouse(Vector2 direction)
    {
        // Switch the way the player is labelled as facing.
        m_FacingRight = !m_FacingRight;
        m_FacingUp = !m_FacingUp;
    }

    private void faceToPlayer()
    {
        if(CurrentPlayer != null)
        {
            if(animator.GetBool("isDead") == false)
            {
                Vector3 lookPos = CurrentPlayer.transform.position;
                lookPos = lookPos - transform.position;
                float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }
        }
    }

    public void killPlayer()
    {
        animator.SetBool("isDead", true);
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0;
        HitDamage = 0;
        Destroy(GetComponent<BoxCollider2D>());
        GetComponent<SpriteRenderer>().sortingLayerName = "Background";
        //play kill sound
        AudioController _audio = gameObject.GetComponent<AudioController>();
        if(_audio != null)
        {
            _audio.playClip(killSound, false, false, 1, false);
        }
        //destroy object
        Invoke("destroPlayer", VanishAfterSeconds);
    }

    public void respawnPlayer()
    {
        //do nothing here
    }


    public void destroPlayer()
    {
        //TODO: check this
        //does this work?
        Destroy(gameObject);
    }
}