﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {
    RectTransform healthBar;
    public const float MIN_HEALTH_VAL = 0;
    public const float MAX_HEALTH_VAL = 1;
    public GameObject UI_HealthBar;
    public GameObject life1;
    public GameObject life2;
    public GameObject life3;
    public GameObject life_full_1;
    public GameObject life_full_2;
    public GameObject life_full_3;

    // Use this for initialization
    void Start () {
        healthBar = GetComponent<RectTransform>();
	}

    //ranges from 0 = 0% to 1 = 100%
    public void setHealthBar(float val)
    {
        if( val >= 0 && val <=1 && healthBar != null)
        {
            if(val >= 1 || val >(2f/3f))
            {
                life_full_1.SetActive(true);
                life_full_2.SetActive(true);
                life_full_3.SetActive(true);
            } else if(val <= (2f/3f) && val > (1f/3f))
            {
                //
                life_full_1.SetActive(false);
                life_full_2.SetActive(true);
                life_full_3.SetActive(true);
            } else if(val <= (1f/3f) && val > 0)
            {
                //
                life_full_1.SetActive(false);
                life_full_2.SetActive(false);
                life_full_3.SetActive(true);
            } else
            {
                //dead
                life_full_1.SetActive(false);
                life_full_2.SetActive(false);
                life_full_3.SetActive(false);
            }
        }
            
    }

    /// <summary>
    /// Show or hide the HealthBar UI
    /// </summary>
    /// <param name="display"></param>
    public void setUIDisplay(bool display)
    {
        if(UI_HealthBar != null)
        {
            UI_HealthBar.SetActive(display);
        }
    }
}
