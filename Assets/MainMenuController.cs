﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// MainMenu UI-Controller
/// </summary>
public class MainMenuController : MonoBehaviour {
    private bool isOpen;
    public bool IsOpen { get; set; }
    public GameObject menuPanel;
    public GameObject highScorePanel;
    public Button StartBtn;
    public Button ExitBtn;
    public Button HighScoreBtn;
    public Button HighScoreBackBtn;
    public Button ResumeBtn;
    public AudioController m_audio;
    bool isStartup = true;
    public PlayerHealthController healthController;
    public GameController game;
    public GameObject dialogScreen;
    public ScoreController scoreController;

    private void Start()
    {
        isStartup = true;
        menuPanel.SetActive(true);
        m_audio.playIntroSong();

        //disable resume button
        ResumeBtn.gameObject.SetActive(false);

        StartBtn.onClick.AddListener(() =>
        {
            isStartup = false;
            dialogScreen.SetActive(true);
            menuPanel.SetActive(false);
        });

        ExitBtn.onClick.AddListener(() =>
        {
            game.QuitGame();
        });

        HighScoreBtn.onClick.AddListener(() =>
        {
            game.OpenHighScore();
        });

        HighScoreBackBtn.onClick.AddListener(() =>
        {
            menuPanel.SetActive(true);
            highScorePanel.SetActive(false);
        });

        ResumeBtn.onClick.AddListener(() =>
        {
            game.ResumeGame();
        });

        healthController.healthBar.setUIDisplay(false);
        scoreController.setUIDisplay(false);
    }

    public void openMenu()
    {
        Cursor.visible = true;
        m_audio.playReenter();
        IsOpen = true;
        menuPanel.SetActive(true);
        healthController.healthBar.setUIDisplay(false);
        scoreController.setUIDisplay(false);

        if(isStartup == true)
        {
            StartBtn.gameObject.SetActive(true);
            ResumeBtn.gameObject.SetActive(false);
        } else
        {
            StartBtn.gameObject.SetActive(false);
            ResumeBtn.gameObject.SetActive(true);
        }
    }

    public void closeMenu()
    {
        Cursor.visible = false;
        m_audio.StopMusic();
        IsOpen = false;
        menuPanel.SetActive(false);
        m_audio.playExitSFX();
        healthController.healthBar.setUIDisplay(true);
        scoreController.setUIDisplay(true);
    }

}
