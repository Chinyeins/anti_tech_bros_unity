﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorMovement : MonoBehaviour {
    public GameObject player;
    public Vector3 mousePosition;
    public float moveSpeed = 1f;
    public float minDist = 1.5f;
    public float maxDist = 5.5f;
    public Vector3 mousePlayerVec = new Vector3();
    public float playerCursorDistance;
    Transform mouseFixed;
    public Vector3 allowedMousePos = new Vector3();
    public float angle;
    public float angleD; //in Degrees
    public int quadrant = 0;
    

    private void Start()
    {
        mouseFixed = this.transform;
    }

    // Update is called once per frame
    void FixedUpdate() { 
        //set cursor to mouse position
        mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        mouseFixed.transform.position = mousePosition - player.transform.position; //Richtungsvektor player Maus
        mousePlayerVec = mouseFixed.transform.position;
        playerCursorDistance = Mathf.Sqrt(Mathf.Pow(mousePlayerVec.x, 2) + Mathf.Pow(mousePlayerVec.y, 2));
        //in Radian
        angle = Mathf.Atan(mousePlayerVec.y / mousePlayerVec.x);

        //check quadrant
        if(mousePlayerVec.x > 0 && mousePlayerVec.y > 0)
        {
            //both ++ => I. Quadrant
            quadrant = 1;
            angleD = angle * (180 / Mathf.PI);
        }
        if (mousePlayerVec.x < 0 && mousePlayerVec.y > 0)
        {
            //both -+ => II. Quadrant
            quadrant = 2;
            angleD = (angle * (180 / Mathf.PI)) + 180;
        }
        if (mousePlayerVec.x < 0 && mousePlayerVec.y < 0)
        {
            //both -- => III. Quadrant
            quadrant = 3;
            angleD = (angle * (180 / Mathf.PI)) + 180;
        }
        if (mousePlayerVec.x > 0 && mousePlayerVec.y < 0)
        {
            //both +- => IV. Quadrant
            quadrant = 4;
            angleD = (angle * (180 / Mathf.PI)) + 360;
        }
        

        //update only if mouse is in boundaries
        if (playerCursorDistance >= minDist && playerCursorDistance <= maxDist)
        {
            allowedMousePos = mousePosition;
        } else
        {
            float r = (playerCursorDistance < minDist) ? minDist : maxDist;
            float fi = angleD * (Mathf.PI / 180);
            allowedMousePos.x = r * Mathf.Cos(fi);
            allowedMousePos.y = r * Mathf.Sin(fi);
            allowedMousePos = player.transform.position + allowedMousePos;
        }

        transform.position = allowedMousePos;
        //set Cursor to mouse pos

        Debug.DrawLine(player.transform.position, transform.position);
    }
}
