﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class isPLayerInSigh : MonoBehaviour {
    public EnemyMoveController enemyMoveController;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            enemyMoveController.IsPLayerInSight = true;
            //set current player as targert
            enemyMoveController.controller.CurrentPlayer = collision.gameObject; 
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
            enemyMoveController.IsPLayerInSight = false;
    }
}
