﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class closScreen : MonoBehaviour {
    public Button btn;
    public GameObject dialogView;
    public GameController game;
    AudioSource audioDialog;
    public MainMenuController menu;

	// Use this for initialization
	void Start () {
        Cursor.visible = true;
        btn.onClick.AddListener(() =>
        {
            dialogView.SetActive(false);
            Cursor.visible = false;
            audioDialog.Stop();
            game.StartGame();
        });

        audioDialog = GetComponent<AudioSource>();
        menu.m_audio.StopMusic();
	}
}
